# NetEye voting only node

## Create a voting-only node in a new cluster
### Initial setup
This software converts a standard NetEye installation in a voting-only installation.
You need 3 neteye nodes configured as a cluster: all cluster-configuration script have been executed, no neteye_secure_install has been executed.

Suppose to have the following configuration:
* node-1 and node-2 are standard NetEye node
* node-3 is the node to be converted to voting-only

### Installation procedure
* Put node-2 and node 3 in standby `pcs node standby node-2.neteyelocal node-3.neteyelocal`
* Execute `neteye_secure_install` on node-1 (active)
* Execute `neteye_secure_install` on node-2 (standby)
* Change `/etc/neteye-cluster` converting the section of node-3 to set it as **VotingOnlyNode**
```yml
{
   "ClusterInterface" : "ens224",
   "Created" : 1580312595,
   "Hostname" : "neteye-is-supercool.neteye.it",
   "Nodes" : [
      {
         "addr" : "192.168.206.1",
         "hostname" : "node-1.neteyelocal",
         "hostname_ext" : "node-1.neteye.it",
         "id" : 1
      },
      {
         "addr" : "192.168.206.2",
         "hostname" : "node-2.neteyelocal",
         "hostname_ext" : "node-2.neteye.it",
         "id" : 2
      }
      ],
      "VotingOnlyNode": {
         "addr" : "192.168.206.3",
         "hostname" : "node-3.neteyelocal",
         "hostname_ext" : "node-3.neteye.it",
         "id" : 3
      }
}
```
* Validate configuration: `cat /etc/neteye-cluster | jq`
* Execute `sh make_me_vote.sh`
* Perform checks suggested in the confluence page
* Execute `neteye_secure_install_voting_only`
* Unstandby all nodes

## Additional documentation
Internal Documentation:
* [NetEye Voting-only node on Confluence](https://siwuerthphoenix.atlassian.net/wiki/spaces/SCN/pages/1436254209)

References:
* [DRBD Arbitrators](https://www.linbit.com/en/cheap-votes-drbd-diskless-quorum/)
* [PCS Quorum Device](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/high_availability_add-on_reference/s1-quorumdev-haar)
* [Voting-only master-eligible node](https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-node.html#voting-only-node)