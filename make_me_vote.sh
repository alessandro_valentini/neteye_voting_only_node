#! /bin/bash
. /usr/share/neteye/scripts/rpm-functions.sh

function get_voting_only_node_arg() {
    ARG=$1
    if ! $(verify_cluster_conf_file); then
        return 1
    else
        echo $(cat /etc/neteye-cluster  | jq -r ".VotingOnlyNode | .$ARG")
        return 0
    fi
}

# Check configuration
IFS=' ' read -ra NE_NODES <<< "$(get_cluster_nodes_hostname)"
VOTING_ONLY=$(get_voting_only_node_arg "hostname" )
VOTING_ONLY_EXT=$(get_voting_only_node_arg "hostname_ext" )

if [[ ${#NE_NODES[@]} < 2  ]]; then
   echo ${NE_NODES[@]}
   echo "The cluster should have at least 2 standard nodes! Got:${#NE_NODES[@]}"
   #exit 1
fi

if [[ -z $VOTING_ONLY ]] || [[ "$VOTING_ONLY" ==  "null"  ]]; then
   echo "The cluster must have exactely one voting only node"
   exit 1
fi

# Configuring DRBD
echo "[+] Configure DRBD"
echo "[+] Make DRBD resources Diskless on node $VOTING_ONLY"
for r in $( ls /etc/drbd.d/*.res ); do
    echo "[i] Configuring $r"  
    sed -i s/"on $VOTING_ONLY_EXT {"/"on $VOTING_ONLY_EXT {\n\tvolume 0 {\n\t\tdisk none;\n\t}"/g ${r}
done

drbdadm adjust all

for NODE in "${NE_NODES[@]}"
do
    echo "[+] Synchronize resources with $NODE"
    rsync -azv /etc/drbd.d/*.res $NODE:/etc/drbd.d/
    ssh $NODE "drbdadm adjust all"
done

echo "[+] DRBD configuration completed"

# Configuring PCS
echo "[+] Configure PCS"
for NODE in "${NE_NODES[@]}"
do
    echo "[i] Install corosync-qdevice on $NODE"
    ssh $NODE "yum install corosync-qdevice -y"
    echo "[i] done"
done

echo "[+] Add collocation constraints"
for r in $(pcs resource group list | grep -o ".*_group" | sed s/_group//g); do
    pcs constraint location ${r} avoids $VOTING_ONLY;
done
echo "[i] done"

echo "[+] Remove $VOTING_ONLY from PCS cluster"
pcs cluster node remove $VOTING_ONLY
ssh ${NE_NODES[0]} "pcs quorum expected-votes ${#NE_NODES[@]}"
echo "[i] done"

echo "[+] Install quorum device on this node"
yum install pcs corosync-qnetd -y
systemctl restart pcsd
pcs qdevice setup model net --enable --start
ssh ${NE_NODES[0]} "pcs quorum device add model net host=$VOTING_ONLY algorithm=ffsplit"
echo "[i] Done"


# Configure elasticsearch
ELASIC_CFG="/neteye/local/elasticsearch/conf/elasticsearch_sg.yml"

if [[ -f "$ELASIC_CFG" ]]; then
    echo "[+] Configuring elasticsearch voting only node"

    echo "node.master: true" >> $ELASIC_CFG
    echo "node.data: false" >> $ELASIC_CFG
    echo "node.ingest: false" >> $ELASIC_CFG
    echo "node.voting_only: true" >> $ELASIC_CFG
    echo "node.ml: false" >> $ELASIC_CFG

    echo "[+] Done"
else
    echo "[i] Elasticsearch not installed on this node."
fi

# Create folder and script for neteye_secure_install_voting_only
sh configure_secure_install.sh