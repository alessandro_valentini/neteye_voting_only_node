#! /bin/bash

# Configure neteye_secure_install
# This part should be done in RPM spec file
echo "[i] Creating symlink for neteye_secure_install_voting_only directory"
mkdir -p /usr/share/neteye/secure_install_voting_only/
cp /usr/share/neteye/secure_install_elastic_only/* /usr/share/neteye/secure_install_voting_only/
## Mofidy yyy_restart_services to view voting-only node as elastic-only node (elasticsearch is the only service running on this node)
sed -i s%". /usr/share/neteye/scripts/rpm-functions.sh"%". /usr/share/neteye/scripts/rpm-functions.sh\nfunction is_es_only(){\n\treturn 0\n}"%g /usr/share/neteye/secure_install_voting_only/yyy_restart_services.sh
ln -s /usr/share/neteye/secure_install/075_configure_drbd.sh /usr/share/neteye/secure_install_voting_only/

echo "[i] configuring secure install"
cp 000_disable_secure_install.sh /usr/share/neteye/secure_install/
cp 000_disable_secure_install.sh /usr/share/neteye/secure_install_elastic_only/
cp neteye_secure_install_voting_only /usr/sbin/

echo "[+] Done"

echo "[!] Please configure this node as elastic-only node and execute neteye_secure_install_voting_only"