#! /bin/bash

echo "[!] neteye_secure_install has been disabled on this server because this is a NetEye Voting Only server and is not supported by standard installer!"
echo "[i] please use instade neteye_secure_install_voting_only instead"

exit 1