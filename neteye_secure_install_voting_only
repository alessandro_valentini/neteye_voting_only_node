#!/bin/bash

# Workaround to cope with RPM functions which is missing support to voting-only-node
# This should be a case in OR with is_elastic_only and should follow the same step in service restart (yyy autosetup)
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Add Voting only nodes to the array of cluster nodes, this override original function in order to fix
# certificate generation. This function should be put in rps-functions.sh
function get_cluster_and_elastic_only_node_arg() {
    ARG=$1
    if ! $(verify_cluster_conf_file); then
        return 1
    else
        NODES=$(cat $NODES_CONF_FILE  | jq -r ".Nodes,.ElasticOnlyNodes | .[]?.$ARG")
        VOTE=$(cat /etc/neteye-cluster  | jq -r ".VotingOnlyNode | .$ARG")
        echo $NODES $VOTE
        return 0
    fi
}

function get_cluster_local_arg() {
    ARG=$1
    if ! $(verify_cluster_conf_file); then
        return 1
    else
        NODE="$(uname -n)"
        NODES=$(cat $NODES_CONF_FILE  | jq -r ".Nodes,.ElasticOnlyNodes | .[]? | select(.hostname_ext|test(\"$NODE\")).$ARG")
        VOTE=$(cat $NODES_CONF_FILE  | jq -r ".VotingOnlyNode | .? | select(.hostname_ext|test(\"$NODE\")).$ARG")

        if [[ -n ${NODES} ]]; then
            echo ${NODES}
        fi

        if [[ -n ${VOTE} ]]; then
            echo ${VOTE}
        fi
        return 0
    fi
}


NE_SCRIPT_DIR=/usr/share/neteye/secure_install/
VOTING_ONLY_SCRIPT_DIR=/usr/share/neteye/secure_install_voting_only/
SECURE_INSTALL_VARDIR=/neteye/shared/secure_install/

. /etc/environment
. $NE_SCRIPT_DIR/functions.sh
. /usr/share/neteye/scripts/rpm-functions.sh


function execute_scripts() {
  if [[ -z "$1" || ! -d "$1" ]] ; then
    echo "[-] Scripts directory parameter is missing or is not a directory"
    exit 1
  fi
  SCRIPT_DIR="$1"
  for script in $SCRIPT_DIR[0-9,y,x]*; do
    s_dir="$(dirname "$script")"
    s_name="$(basename "$script")"
    echo -e "$BOLD$GREEN[!]$NEUTRAL RUNNING ${s_dir}/$BOLD$GREEN${s_name}$NEUTRAL"
    # All autosetup script receives script name as first parameter
    source $script "$s_name"
  done
}

if is_cluster ; then
    echo "[i] Checking SSH access"
    for node in $(get_cluster_nodes_hostname) ; do
        echo -n "$node "
        ssh $node 'echo "OK"'
    done

    RUNNING_SERVICES=""
    ENABLED_SERVICES=""
else
    RUNNING_SERVICES="influxdb mariadb grafana-server"
    ENABLED_SERVICES="neteye.target"
fi

for service in $RUNNING_SERVICES; do
    systemctl status $service >/dev/null 2>&1
    RESULT=$?
    if [ $RESULT != "0" ]; then
       echo "Service '$service' must be running for initial setup!" 1>&2
       exit $RESULT
    fi
done

if [ -t 1 ] ; then
    BOLD="\e[1m"
    RED="\e[31m"
    GREEN="\e[32m"
    YELLOW="\e[33m"
    NEUTRAL="\e[0m"
else
    BOLD=""
    RED=""
    GREEN=""
    YELLOW=""
    NEUTRAL=""
fi

execute_scripts $VOTING_ONLY_SCRIPT_DIR


echo "[i] Enabling crucial services"
for service in $ENABLED_SERVICES ; do
    systemctl disable $service
    systemctl enable $service
done
echo "[+] Done."

exit 0
